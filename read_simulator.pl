#!/usr/bin/perl

use warnings;
use strict;
use v5.8.7;

use Bio::DB::Fasta or die "Be sure that Bio::DB::Fasta is installed from cpan\n";
use File::Basename or die "Be sure that File::Basename is installed from cpan\n"

sub usage{
print <<EOF
usage: perl read_simulator <fasta file> <optional fragment length (200)> <optional read length (50)>

This script uses the information within a genome wide fasta file to generate a uniform distribution of 
sequence reads simulating something like RNA-Seq. 

EOF
}

# print the usage unless we have a valid fasta file as an arg
usage() unless scalar @ARGV > 0;

# read in the fasta file name 
my ($filename,$directory,$suffix) = fileparse(shift, qr/\.[^.]*/);
# read in optional fragment length
my $frag_length = shift || 200;
# read in the optional read length
my $read_length = shift || 50;
# open the two paired end read files
open my $R1, ">", basename($filename)."_R1.fastq" or die $!;
open my $R2, ">", basename($filename)."_R2.fastq" or die $!;


# make the fasta database
my $fasta = Bio::DB::Fasta->newFh($filename.$suffix) or die "Please check that your fasta file is valid\n" . usage();

# Fastq file entries look like the following:
# @HWI-ST261:406:D0DPHACXX:5:1101:1364:2142#TGACCA/1
# GGGCATGATCGTTTTGCCTGCAGGAATGTATCATCGCTTTACGTTGGATA
# +
# CCCFFFFFHHHHHJJJJJJJJJJJJJIJHIJIJJJJJJJJIJJJJJJJJI

# Where the first line is the identifier separated by ":"
# HWI-ST261       the unique instrument name
# 406             the run id
# D0DPHACXX       The flow cell id
# 5               flowcell lane
# 1101            tile number within the flowcell lane
# 1364            x-coordinate of the cluster within the tile
# 2142            y-coordinate of the cluster within the tile
# #TGACCA         The index sequence for multiplexing
# \1              the member of the mate pair (\1 or \2 for paired end or mate pair reads only)      

# The second line contains the raw read sequence
# The third line is just a plus sign
# The fourth line contains an encoded quality score for the sequence in line 2

my $x_coor = 1;
my $y_coor = 1;

# Here is our big loop, right now we use an iterator so we don't have to read in the whole fasta file
while(my $gene = <$fasta>){
    my $glength = $gene->length;
    foreach my $index (1..($glength-$frag_length)){
        # Print the Forward Mate
        my $fwd_index = $index;
        my $fwd_targ_index = ($fwd_index+$read_length-1);
        print $R1 '@READ-SIMULATER'.":$gene:$fwd_index:$fwd_targ_index:$read_length:$x_coor:$y_coor".'#GATTAGA\1'."\n";
        print $R1 $gene->subseq($fwd_index => $fwd_targ_index)."\n";
        print $R1 "+"."\n";
        print $R1 ("J"x$read_length)."\n";
        # print the Reverse Mate
        my $rev_index = ($index+$frag_length)-$read_length;
        my $rev_targ_index = ($rev_index+$read_length-1);
        print $R2 '@READ-SIMULATER'.":$gene:$rev_index:$rev_targ_index:$read_length:$x_coor:$y_coor".'#GATTAGA\2'."\n";
        print $R2 rev_comp($gene->subseq($rev_index => $rev_targ_index))."\n";
        print $R2 "+"."\n";
        print $R2 ("J"x$read_length)."\n";
        # update coordinates for mate pairs
        $x_coor += 1;
        $y_coor += 1;
        # Print a handy message
        print STDERR "On Iteration $x_coor\n";
    }
}


# DNA Sense  ( Via Wikipedia)
# Molecular Biologists call a single strand of DNA sense (or positive (+) ) if an RNA version of the same sequence
# is translated or translatable into protein.  Its complementary strand is called antisense (or negative (-) sense). 
# Sometimes the phrase coding strand  is encountered; however, protein coding and non-coding RNA's can be transcribed
# similarly from both strands, in some cases being transcribed in both directions from a common promoter region,
# or being transcribed from within introns, on both strands (see "ambisense" below).

# When we make a read, we are looking at the following:
# http://www.illumina.com/Images/technology/mate_pair_sequencing_lg.gif

# So when we are simulating a sequence from a fasta file such as:
#    
#      >GRMZM2G055768 seq=gene; coord=9:130812842..130820923:1
#      ATGAGCGACTCCCAAGCCCAAATCACCGTCCGCCGCCGCGCGAGGTAAGTTCCTGCCCAT

# The actual DNA that gets sequenced looks like
#
#            11111111 -> ATGAGCGACTCCCAAGCCCAAATCACCGTCCGCCGCCGCGCGAGGTAAGTTCCTGCCCAT
#                        TACTCGCTGAGGGTTCGGGTTTAGTGGCAGGCGGCGGCGCGCTCCATTCAAGGACGGGTA <- 22222222

# Where 111111 is the primer for the first mate pair and 222222 is the primer for the second mate pair
# We would expect to get sequence as follows (5 base pair reads for example):

# ATGAG (from the 1st mate pair)
# ATGGG (from the 2nd mate pair)

# Where the second mate pair is the reverse complement in releation to the first mate


# Computes the reverse complement of a sequence
# Input : a sequence string
# Output: reverse complemented string
sub rev_comp {
    my $seq = shift;
    $seq = reverse($seq);
    $seq =~ tr/ACGTacgt/TGCAtgca/;
    return($seq);
}

