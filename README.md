bam2counts
==========

Overview
--------

This is the beginning of a suite of tools which will incorporate a maize specific
RNA-Seq pipeline. 

### bam2counts.sh

Generates a tab delimited read count file from RNAseq bam alignment file.

Requires the use of BED tools and SAM tools


